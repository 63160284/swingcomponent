/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.swingcomponent;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author Thanya
 */
public class FirstSwingExample {
    public static void main(String[] args) {
        JFrame f = new JFrame();
        
        JButton btn = new JButton("click");
        btn.setBounds(130, 100, 100, 40);
        
        f.add(btn);
        
        f.setSize(400, 500);
        f.setLayout(null);
        f.setVisible(true);
    }
}
